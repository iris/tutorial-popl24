From iris_tutorial Require Import session_channel.

(* This example is similar to "oneshot_chan_prog" in one_shot_examples.v,
but uses the session channels instead of one-shot channels. *)
Definition session_prog : val := λ: <>,
  let: "c" := new #() in
  Fork (let: "lc" := recv "c" in
        let: "l" := Fst "lc" in
        let: "c'" := Snd "lc" in
        "l" <- !"l" + #2;;
        close "c'");;
  let: "l" := ref #40 in
  let: "c'" := send "c" "l" in
  wait "c'";;
  let: "x" := !"l" in Free "l";; "x".

Section session_proofs.
  Context `{!heapGS Σ, !chanG Σ}.
  Notation prot := (prot Σ).

  (* This protocol specifies that we first send a location [l], as well as the
  information [l ↦ #40], which says that the reference contains 40.
  We then receive the END token, and [l ↦ #42], which specifies that
  the other side must update the reference to 42. *)
  Definition session_prog_pre_prot : prot :=
    <! (l : loc)> MSG #l {{ l ↦ #40 }};
    <?> END{{ l ↦ #42 }}.

  Lemma session_prog_pre_spec :
    {{{ True }}} session_prog #() {{{ RET #42; True }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_smart_apply (new_spec session_prog_pre_prot); [done|].
    iIntros (c) "[Hc1 Hc2]".
    wp_smart_apply (wp_fork with "[Hc2]").
    - iIntros "!>". rewrite /session_prog_pre_prot send_prot_dual.
      wp_smart_apply (recv_spec with "Hc2") as (l c') "[Hc Hl] /=".
      wp_pures. wp_load. wp_store. rewrite wait_prot_dual.
      wp_apply (close_spec with "[$Hc $Hl]") as "_". done.
    - wp_alloc l as "Hl".
      wp_pures.
      wp_apply (send_spec l with "[$Hc1 $Hl]") as (c') "Hc1".
      wp_smart_apply (wait_spec with "Hc1") as "Hl".
      wp_load. wp_free. wp_pures. by iApply "HΦ".
  Qed.

  Definition session_prog_prot : prot :=
    <! (lx : loc * Z)> MSG #lx.1 {{ lx.1 ↦ #lx.2 }};
    <?>END{{ lx.1 ↦ #(lx.2 + 2) }}.

  Lemma session_prog_spec :
    {{{ True }}} session_prog #() {{{ RET #42; True }}}.
  (* SOLUTION *) Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_smart_apply (new_spec session_prog_prot); [done|].
    iIntros (c) "[Hc1 Hc2]".
    wp_smart_apply (wp_fork with "[Hc2]").
    - iIntros "!>". rewrite /session_prog_prot. rewrite send_prot_dual.
      wp_smart_apply (recv_spec with "Hc2") as ([l x] c') "[Hc Hl] /=".
      wp_pures. wp_load. wp_store. rewrite wait_prot_dual.
      wp_apply (close_spec with "[$Hc $Hl]") as "_". done.
    - wp_alloc l as "Hl".
      wp_pures.
      wp_apply (send_spec (l,40%Z) with "[$Hc1 $Hl]") as (c') "Hc1".
      wp_smart_apply (wait_spec with "Hc1") as "Hl".
      wp_load. wp_free. wp_pures. by iApply "HΦ".
  Qed.
End session_proofs.

Definition session_imp_prog : val := λ: <>,
  let: "cs" := new_imp #() in
  let: "c1" := Fst "cs" in
  let: "c2" := Snd "cs" in
  Fork (let: "l" := recv_imp "c2" in
        "l" <- !"l" + #2;;
        close_imp "c2");;
  let: "l" := ref #40 in
  send_imp "c1" "l";;
  wait_imp "c1";;
  let: "x" := !"l" in Free "l";; "x".

Section imp_example.
  Context `{!heapGS Σ, !chanG Σ}.

  Lemma session_imp_prog_spec :
    {{{ True }}} session_imp_prog #() {{{ RET #42; True }}}.
  (* SOLUTION *) Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_smart_apply (new_imp_spec session_prog_prot); [done|].
    iIntros (c1 c2) "[Hc1 Hc2]".
    wp_smart_apply (wp_fork with "[Hc2]").
    - iIntros "!>".
      rewrite /session_prog_prot send_prot_dual.
      wp_smart_apply (recv_imp_spec with "Hc2") as ([l x]) "[Hc Hl] /=".
      wp_pures. wp_load. wp_store.
      wp_apply (close_imp_spec with "[$Hc $Hl]") as "Hc''". done.
    - wp_alloc l as "Hl".
      wp_pures.
      wp_apply (send_imp_spec (l,40%Z) with "[$Hc1 $Hl]") as "Hc1".
      wp_smart_apply (wait_imp_spec with "Hc1") as "Hl".
      wp_load. wp_free. wp_pures. by iApply "HΦ".
  Qed.
End imp_example.

Definition session_imp_rec_prog : val := λ: <>,
  let: "cs" := new_imp #() in
  let: "c1" := Fst "cs" in
  let: "c2" := Snd "cs" in
  Fork ((rec: "go" <> :=
          if: recv_imp "c2"
          then let: "l" := recv_imp "c2" in
               "l" <- (!"l" + #2);;
               send_imp "c2" #();; "go" #()
          else close_imp "c2") #());;
  let: "l" := ref #38 in
  send_imp "c1" #true;; send_imp "c1" "l";; recv_imp "c1";;
  send_imp "c1" #true;; send_imp "c1" "l";; recv_imp "c1";;
  send_imp "c1" #false;; wait_imp "c1";;
  let: "x" := !"l" in Free "l";; "x".

Section imp_rec_example.
  Context `{!heapGS Σ, !chanG Σ}.

  Definition session_prog_prot_rec_aux p : prot Σ :=
    <! (b:bool)> MSG #b ;
    if b
    then <! (lx : loc * Z)> MSG #lx.1 {{ lx.1 ↦ #lx.2 }};
         <?> MSG #() {{ lx.1 ↦ #(lx.2 + 2) }}; p
    else <?>END.

  Global Instance session_prog_prot_rec_aux_contractive :
    Contractive session_prog_prot_rec_aux.
  Proof.
    unfold session_prog_prot_rec_aux.
    intros n p1 p2 Hpeq. eapply send_prot_ne; [done..|].
    repeat intro. destruct a.
    - eapply send_prot_ne; [done..|]. repeat intro.
      eapply recv_prot_contractive; try done.
      solve_proper.
    - done.
  Qed.
  Definition session_prog_prot_rec := fixpoint session_prog_prot_rec_aux.
  Lemma session_prog_prot_rec_unfold : session_prog_prot_rec ≡
     session_prog_prot_rec_aux session_prog_prot_rec.
  Proof. rewrite /session_prog_prot_rec. apply fixpoint_unfold. Qed.

  Lemma session_imp_rec_prog_spec :
    {{{ True }}}
      session_imp_rec_prog #()
    {{{ RET #42; True }}}.
  (* SOLUTION *) Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_smart_apply (new_imp_spec session_prog_prot_rec); [done|].
    iIntros (ch1 ch2) "[Hc1 Hc2]".
    wp_smart_apply (wp_fork with "[Hc2]").
    - iIntros "!>". wp_pure _.
      iLöb as "IH" forall (ch2).
      rewrite {2}session_prog_prot_rec_unfold.
      rewrite /session_prog_prot_rec_aux.
      rewrite send_prot_dual.
      wp_smart_apply (recv_imp_spec with "Hc2").
      iIntros ([]) "[Hc2 _] /=".
      + rewrite send_prot_dual.
        wp_smart_apply (recv_imp_spec with "Hc2").
        iIntros ([l x]) "[Hch2 Hl] /=". rewrite recv_prot_dual.
        wp_load. wp_store.
        wp_smart_apply (send_imp_spec () with "[$Hch2 $Hl]") as "Hch2".
        do 2 wp_pure. by iApply "IH".
      + rewrite wait_prot_dual.
        wp_smart_apply (close_imp_spec with "[$Hc2 //]") as "_". done.
    - rewrite {1}session_prog_prot_rec_unfold.
      wp_alloc l as "Hl".
      wp_smart_apply (send_imp_spec with "[$Hc1 //]") as "Hc1".
      wp_smart_apply (send_imp_spec (l,38%Z) with "[$Hc1 $Hl]") as "Hc1".
      wp_smart_apply (recv_imp_spec with "Hc1") as "_ [Hc1 Hl] /=".
      replace #(38 + 2) with #40; last by do 2 f_equiv; lia.
      rewrite {1}session_prog_prot_rec_unfold.
      wp_smart_apply (send_imp_spec with "[$Hc1//]") as "Hc1".
      wp_smart_apply (send_imp_spec (l,40%Z) with "[$Hc1 $Hl]") as "Hc1".
      wp_smart_apply (recv_imp_spec with "Hc1") as "_ [Hc1 Hl] /=".
      rewrite {1}session_prog_prot_rec_unfold.
      wp_smart_apply (send_imp_spec with "[$Hc1//]") as "Hc1".
      wp_smart_apply (wait_imp_spec with "Hc1") as "_".
      wp_load. wp_free. wp_pures. by iApply "HΦ".
  Qed.
End imp_rec_example.
