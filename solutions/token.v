From iris.algebra Require Import excl.
From iris.heap_lang Require Export proofmode notation.

Class tokG Σ := TokG { tokG_inG : inG Σ (excl unit) }.
Local Existing Instance tokG_inG.
Definition tokΣ : gFunctors := #[GFunctor (exclR unit)].
Global Instance subG_tokΣ {Σ} : subG tokΣ Σ → tokG Σ.
Proof. solve_inG. Qed.

Definition tok `{!tokG Σ} (γ : gname) : iProp Σ := own γ (Excl ()).

Lemma tok_alloc `{!tokG Σ} : ⊢ |==> ∃ γ, tok γ.
Proof. by iApply own_alloc. Qed.

Lemma tok_excl `{!tokG Σ} γ : tok γ -∗ tok γ -∗ False.
Proof. iIntros "H1 H2". iDestruct (own_valid_2 with "H1 H2") as %[]. Qed.
