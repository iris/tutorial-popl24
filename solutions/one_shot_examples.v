(** This file contains examples that use the one-shot API from one_shot.v. *)
From iris_tutorial Require Import one_shot.

(** This program creates a one-shot channel "c", forks off a thread that sends
[42] on "c", and receives this message in the main thread. *)
Definition oneshot_prog : val := λ: <>,
  let: "c" := new1 #() in
  Fork (send1 "c" #42);;
  recv1 "c".

(** This program is similar to the previous example, but instead of sending [42]
directly, we send a mutable reference that contains [42]. *)
Definition oneshot_ref_prog : val := λ: <>,
  let: "c" := new1 #() in
  Fork (let: "l" := ref #42 in
        send1 "c" "l");;
  let: "l" := recv1 "c" in
  let: "x" := !"l" in
  Free "l";; "x".

(** This program does two-step communication:
- The main thread creates a reference, as well as a continuation channel "c2",
  and sends this as a pair to the forked-off thread.
- The forked-off thread receives this pair, and extracts the reference "l" and
  the continuation channel "c2".
- The forked-off thread then increments the reference "l" by 2, and sends an
  acknowledgement on the continuation channel "c2".
- The main thread then receives this acknowledgement on "c2", and reads the
  value of "l". *)
Definition oneshot_chan_prog : val := λ: <>,
  let: "c1" := new1 #() in
  Fork (let: "lc" := recv1 "c1" in
        let: "l" := Fst "lc" in
        let: "c2" := Snd "lc" in
        "l" <- !"l" + #2;;
        send1 "c2" #());;
  let: "l" := ref #40 in
  let: "c2" := new1 #() in send1 "c1" ("l","c2");;
  recv1 "c2";;
  let: "x" := !"l" in
  Free "l";; "x".

Section proof_base.
  Context `{!heapGS Σ, !chanG Σ}.

  Definition oneshot_prot : prot Σ :=
    (false, λ v, ⌜v = #42⌝)%I.

  Lemma oneshot_prog_spec :
    {{{ True }}} oneshot_prog #() {{{ RET #42; True }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_lam.
    (** To apply the one-shot rules (defined in terms of Texan triples) we use
    the [wp_apply] tactic. *)
    wp_apply (new1_spec oneshot_prot with "[//]") as (c) "[Hc1 Hc2]".
    wp_smart_apply (wp_fork with "[Hc2]").
    { iModIntro.
      wp_apply (send1_spec with "[$Hc2 //]") as "_".
      done. }
    wp_smart_apply (recv1_spec with "Hc1") as (v) "->".
    by iApply "HΦ".
  Qed.

  Definition oneshot_ref_prot : prot Σ :=
    (false, λ v, ∃ l : loc, ⌜v = #l⌝ ∗ l ↦ #42)%I.

  Lemma oneshot_ref_prog_spec :
    {{{ True }}} oneshot_ref_prog #() {{{ RET #42; True }}}.
  (* SOLUTION *) Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_smart_apply (new1_spec oneshot_ref_prot with "[//]") as (c) "[Hc1 Hc2]".
    wp_smart_apply (wp_fork with "[Hc2]").
    { iIntros "!>". wp_alloc l as "Hl".
      wp_smart_apply (send1_spec with "[$Hc2 Hl]"); [|done].
      iExists _. by iFrame. }
    wp_smart_apply (recv1_spec with "Hc1") as (v) "(%l & -> & Hl)".
    wp_load. wp_free. wp_pures. by iApply "HΦ".
  Qed.

  Definition oneshot_chan2_prot (l : loc) (x : Z) : prot Σ :=
    (false, λ v, ⌜v = #()⌝ ∗ l ↦ #(x+2))%I.
  Definition oneshot_chan1_prot : prot Σ :=
    (true, λ v, ∃ (l : loc) (x : Z) (c : val),
      ⌜v = (#l, c)%V⌝ ∗ l ↦ #x ∗
      is_chan c (dual (oneshot_chan2_prot l x)))%I.

  Lemma oneshot_chan_prog_spec :
    {{{ True }}} oneshot_chan_prog #() {{{ RET #42; True }}}.
  (* SOLUTION *) Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    wp_smart_apply (new1_spec oneshot_chan1_prot with "[//]")
      as (ch) "[Hc11 Hc12]".
    wp_smart_apply (wp_fork with "[Hc12]").
    { iModIntro.
      wp_smart_apply (recv1_spec with "Hc12") as (v) "Hv /=".
      iDestruct "Hv" as (l x c ->) "[Hl Hc22]".
      wp_pures. wp_load. wp_store.
      by iApply (send1_spec with "[$Hc22 $Hl]"). }
    wp_alloc l as "Hl".
    wp_smart_apply (new1_spec (oneshot_chan2_prot l 40) with "[//]")
      as (c2) "[Hc21 Hc22]".
    wp_smart_apply (send1_spec with "[$Hc11 Hc22 Hl]") as "_".
    { iExists _, _, _. iFrame. done. }
    wp_smart_apply (recv1_spec with "Hc21") as (v) "[-> Hl]".
    wp_load. wp_free. wp_pures. by iApply "HΦ".
  Qed.
End proof_base.
