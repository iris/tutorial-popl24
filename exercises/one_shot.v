(** This file contains the implementation and verification of one-shot
channels. The verification uses Iris's "invariants" and the "token ghost state".
If you are not familiar with Iris yet, it might be recommend to first go through
[one_shot_examples] ad [session_channel] to see how the one-shot channels are
used in higher levels of layers. *)
From iris.base_logic.lib Require Import invariants.
From iris.heap_lang Require Export proofmode notation.
From iris_tutorial Require Import token.

(** We implement one-shot channels in terms of mutable references. One-shot
channels have three operations:
1. [let c = new1 () in ..] -- Returns a new one-shot channel.
2. [send1 c v] -- Sends the message [v] on [c].
3. [recv1 c] -- Loops until the other side sends a message on [c], and returns
   the message.
Behind the scenes, one-shot channels are represented as a single mutable
reference that initially contains [NONE]. To send a message, we set it to
[SOME v]. To receive the message, we loop until we see [SOME v], and then we
deallocate the mutable reference and return [v]. *)

Definition new1 : val := λ: <>,
  ref NONE.
Definition send1 : val := λ: "l" "v",
  "l" <- SOME "v".
Definition recv1 : val :=
  rec: "go" "l" :=
    match: !"l" with
      NONE => "go" "l"
    | SOME "v" => Free "l";; "v"
    end.

Definition prot Σ := prod bool (val -d> iProp Σ).

Class chanG Σ := ChanG { chanG_tokG : tokG Σ }.
Local Existing Instance chanG_tokG.
Definition chanΣ : gFunctors := #[tokΣ].
Global Instance chanG_chanΣ {Σ} : subG chanΣ Σ → chanG Σ.
Proof. solve_inG. Qed.

Section proof_base.
  Context `{!heapGS Σ, !chanG Σ}.
  (** Invariants in Iris have a name, which are arranged through namespaces.
  Since we are using a single invariant, the name does not matter. We simply
  pick "chan" in the top-level namespace [nroot]. *)
  Let N := nroot .@ "chan".

  Definition chan_inv (γ1 γ2 : gname) (l : loc)
      (Φ : val → iProp Σ) : iProp Σ :=
    (l ↦ NONEV) ∨ (∃ v, l ↦ SOMEV v ∗ tok γ1 ∗ Φ v) ∨ (tok γ1 ∗ tok γ2).
  (** The later modality [▷] is a technicality, needed for constructive recursive
  protocol definitions through guarded recursion. *)
  Definition is_chan (ch : val) (p : prot Σ) : iProp Σ :=
    ∃ γ1 γ2 (l : loc),
      ⌜ch = #l⌝ ∗
      inv N (chan_inv γ1 γ2 l p.2) ∗
      ▷ tok (if p.1 then γ1 else γ2).

  (** The dual operator, and some of its properties. *)
  Definition dual (p : prot Σ) : prot Σ := (negb p.1, p.2).

  Lemma dual_dual p : dual (dual p) = p.
  Proof. by destruct p as [[] ?]. Qed.

  (** The [Proper] instances below are an Iris technicality, allowing us to
  use one-shot protocols in guarded recursive definitions. *)
  Global Instance chan_inv_ne γ1 γ2 l n :
    Proper (pointwise_relation _ (dist n) ==> dist n) (chan_inv γ1 γ2 l).
  Proof. solve_proper. Qed.
  Global Instance chan_inv_proper γ1 γ2 l :
    Proper (pointwise_relation _ (≡) ==> (≡)) (chan_inv γ1 γ2 l).
  Proof. solve_proper. Qed.

  Global Instance is_chan_contractive ch : Contractive (is_chan ch).
  Proof.
    intros n [??] [??] Hpair; solve_proper_prepare.
    repeat first [f_contractive; destruct Hpair; simplify_eq/=|f_equiv].
  Qed.
  Global Instance is_chan_ne ch : NonExpansive (is_chan ch).
  Proof. solve_proper. Qed.
  Global Instance is_chan_proper ch : Proper ((≡) ==> (≡)) (is_chan ch).
  Proof. solve_proper. Qed.

  Global Instance dual_ne : NonExpansive dual.
  Proof. solve_proper. Qed.
  Global Instance dual_proper : Proper ((≡) ==> (≡)) dual.
  Proof. solve_proper. Qed.

  (** The proofs of the specifications *)
  Lemma new1_spec p :
    {{{ True }}}
      new1 #()
    {{{ ch, RET ch; is_chan ch p ∗ is_chan ch (dual p) }}}.
  Proof.
    iIntros (Ψ) "_ HΨ". wp_lam. wp_alloc l as "Hl".
    iApply "HΨ".
    iMod tok_alloc as (γ1) "Hγ1".
    iMod tok_alloc as (γ2) "Hγ2".
    iMod (inv_alloc N _ (chan_inv γ1 γ2 l p.2) with "[Hl]") as "#?"; [by iLeft|].
    destruct p as [[] ?]; [iSplitL "Hγ1"|iSplitL "Hγ2"];
      iExists _, _; by eauto with iFrame.
  Qed.

  Lemma send1_spec ch P v :
    {{{ is_chan ch (true,P) ∗ P v }}} send1 ch v {{{ RET #(); True }}}.
  Proof.
    iIntros (φ) "[(%γ1 & %γ2 & %l & -> & #Hinv & >Htok) HP] Hφ /=".
    wp_lam; wp_pures.
    iInv N as "[Hl|[(%w & Hl & >Htok' & HΦ')|>[Htok' Htok'']]]".
    - wp_store. iSplitR "Hφ"; last by iApply "Hφ".
      rewrite /chan_inv; eauto 10 with iFrame.
    - iDestruct (tok_excl with "Htok Htok'") as %[].
    - iDestruct (tok_excl with "Htok Htok'") as %[].
  Qed.

  Lemma recv1_spec ch P :
    {{{ is_chan ch (false,P) }}} recv1 ch {{{ v, RET v; P v }}}.
  Proof.
    iIntros (φ) "(%γ1 & %γ2 & %l & -> & #Hinv & >Htok) Hφ /=".
    iLöb as "IH". wp_rec. wp_bind (Load _).
    iInv N as "[Hl|[(%w & Hl & Htok' & HΦ')|>[Htok' Htok'']]]".
    - wp_load. iModIntro. iSplitL "Hl"; [iNext; by iLeft|].
      wp_match. iApply ("IH" with "Htok Hφ").
    - wp_load. iModIntro. iSplitL "Htok Htok'".
      + rewrite /chan_inv. by eauto with iFrame.
      + wp_match. wp_free. wp_seq. by iApply "Hφ".
    - iDestruct (tok_excl with "Htok Htok''") as %[].
  Qed.
End proof_base.

Global Typeclasses Opaque is_chan.
