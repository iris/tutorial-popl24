(** This file gives a brief introduction to HeapLang and its tactics. We mostly
introduce HeapLang by example, providing just enough context to work on the
message-passing examples later. For more information, please consider:

- Exercise 1 and 2 of the POPL'21 tutorial,
  https://gitlab.mpi-sws.org/iris/tutorial-popl21/-/tree/master
- The documentation of HeapLang and its tactics
  https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/heap_lang.md *)

From iris.heap_lang Require Export proofmode notation.

(** Programs in HeapLang are defined with custom (untyped) syntax. The syntax
follows regular Coq conventions, but uses strings (e.g., ["x"]) for names
binders and custom notation ([<>]) for anonymous binders. The syntax uses colons
to distinguish itself from Coq primitives (e.g., [let: "x" := e]). *)
Definition ref_prog : val := λ: <>,
  let: "l" := ref NONEV in
  "l" <- SOMEV #42;;
  let: "v" := !"l" in
  Free "l";; "v".

(** The following program matches on the result of [ref_prog]. In the [NONE]
branch, the unit value is applied to itself, which illegal (i.e., gets stuck in
the operational semantics). In the verification we thus need to show that the
[NONE] branch cannot happen. *)
Definition match_prog : val := λ: <>,
  match: ref_prog #() with
    NONE => #() #()
  | SOME "x" => "x"
  end.

(** A variant of the example above where we free the reference in a new
thread. Compared to the slides, we prefer [Fork] over parallel composition [||]
when doing proofs in Coq. To use parallel composition, use
https://gitlab.mpi-sws.org/iris/iris/-/blob/master/iris_heap_lang/lib/par.v *)
Definition fork_prog : val := λ: <>,
  let: "l" := ref NONEV in
  "l" <- SOMEV #42;;
  let: "v" := !"l" in
  Fork (Free "l");; "v".

(** Iris is parameterized by the type of ghost state that is needed to carry out
a proof. As such, the type of Iris propositions [iProp] is indexed by a [Σ]: a
list of "cameras" (actually, functors from OFEs to cameras). To make our proofs
generic, we abstract over any such [Σ] and use type classes to ensure that the
necessary cameras are present in [Σ].

For this proof, we do not need any special ghost state (i.e., cameras) apart
from the ghost state that HeapLang uses internally for modeling the [l ↦ v]
connective. The cameras for this ghost state are provided by the class
[heapGS]. *)
Section proof.
  Context `{!heapGS Σ}.

  (** Rather than Hoare triples Iris uses "weakest preconditions":

    [ WP e {{ Φ }} ].

  Similar to Hoare triples, the weakest precondition expresses that:
  1. The expression [e] is safe to execute, and,
  2. If it terminates with a value [v] then [Φ v] holds.

  Hoare triples can be defined in terms of weakest preconditions as:

  [ { P } e { Φ } ≜ P -∗ WP e {{ Φ }} ].

  Here [-∗] is the separation implication (i.e., magic wand), which acts is the
  separation-logic version of implication.

  For Coq unification purposes, we define our specifications as
  "Texan triples": [ {{{ P }}} e {{{ RET v, Q }}} ].
  The "Texan triple" [ {{{ P }}} e {{{ RET v, Q }}} ] is syntactic sugar for:

     [ ∀ Φ, P -∗ ▷ (Q -∗ Φ v) -∗ WP e {{ v, Φ v }} ]

  Which is logically equivalent to [ P -∗ WP e {{ x, x = v ∗ Q }} ]

  In practice, the "Texan triple" requires similar effort to prove, but is
  usually easier to use in other proofs, because the postcondition does not have
  to syntactically match [Q]. Using Texan triples, the consequence and framing
  rule is implicitly applied on the postcondition. *)

  Lemma ref_prog_spec :
    {{{ True }}} ref_prog #() {{{ v, RET v; ⌜v = SOMEV #42⌝ }}}.
  Proof.
    (** The Iris Proof Mode (IPM) has tactics similar to Coq, prefixed by *i*
    (e.g., [intros] becomes [iIntros]). IPM supports separation logic reasoning
    using a separate, spatial, separation logic context. The IPM tactics use
    strings for their arguments (e.g., [iIntros "HP HΦ"]). To interact with the
    Coq context one can use parentheses (e.g., [iIntros (Φ)]) or the symbol "%"
    (e.g., [iIntros "%Φ"]). Both can be sequenced in one tactic (e.g.,
    [iIntros (Φ) "HP HΦ"] or [iIntros "%Φ HP HΦ"]). *)
    iIntros (Φ) "HP HΦ".
    (** IPM has tactics for symbolically resolving the primitive HeapLang
    instructions, such as lambdas, reference allocation, storing, loading,
    and freeing. These are prefixed by "wp_", and postfixed with the name
    of the instruction (e.g. [wp_lam]). *)
    wp_lam.
    (** Rules that results in quantified variables and new propositions require
    specifying how these are bound (similar to the [iIntros] instruction). *)
    wp_alloc l as "Hl".
    (** We can resolve pure instructions (e.g., for function application and
    let-bindings) using the tactic [wp_pures]. *)
    wp_pures.
    (** The tactics associated with rules where preconditions can be inferred
    from the instruction (e.g., the store, load, and free rules) automatically
    find the necessary resource in the spatial context. *)
    wp_store. wp_load.
    (** The rules automatically apply any pure rules (e.g., for function
    application and let-bindings) until the specified rule can be applied. *)
    wp_free.
    (** When the remaining program is pure, we can use [wp_pures] to arrive at
    the final obligation to prove the postcondition. *)
    wp_pures.
    (** For technical reasons, Iris often gives goals with the update modality
    [|=>] or later modality [▷]. These modalities make the goal "stronger", but
    in most cases this additional strength is not needed. Both modalities can be
    introduced through [iModIntro]. *)
    (** We can directly apply separation logic rules and implications using
    [iApply]. *)
    iApply "HΦ".
    (** Meta-logic (Coq) propositions are coerced into the separation logic
    with [⌜ _ ⌝]. The prove the goal at Coq level, we can escape the IPM using
    [iPureIntro], whenever the goal is a Coq proposition. *)
    iPureIntro. reflexivity.
  Qed.

  (** Instead of a variable, we can also put the return value directly in the
  argument of [RET]. When possible, we will always do this from now on. *)
  Lemma ref_prog_spec_alt :
    {{{ True }}} ref_prog #() {{{ RET SOMEV #42; True }}}.
  Proof.
    (** Below we shorten the above proof a bit:
    - We omit invocations of [wp_pures], [iPureIntro] and [iModIntro] because
      these are performed automatically.
    - Since the precondition is [True], we discard it using [_].
    - We do not name the resource obtained in [wp_alloc]. *)
    iIntros (Φ) "_ HΦ".
    wp_lam. wp_alloc l. wp_store. wp_load. wp_free.
    wp_pures. by iApply "HΦ".
  Qed.

  Lemma match_prog_spec :
    {{{ True }}} match_prog #() {{{ RET #42; True }}}.
  Proof.
    iIntros (Φ) "_ HΦ". wp_lam.
    (** Here we want to make use of the specification of [ref_prog] that we
    proved above. To use a previously proven specification, Iris provides the
    tactics [wp_apply]. The tactic is used as follows:

      wp_apply (lemma with "[ .. ]") as ".."

    Between the first ".." one can put a "specialization pattern" that gives a
    list of separation logic hypotheses that should be used for proving the
    precondition. When a hypothesis is prefixed with "$", it is framed. If "//"
    is added to the end, the goal is finished by [done].

    Between the second ".." one can put an "introduction pattern" that specifies
    how the postcondition should be introduced into the context.

    For [ref_prog_spec_alt], the precondition is [True], so we use "//"
    immediately. The postcondition is similarly [True], so we discard it using
    the introduction pattern "_". *)
    wp_apply (ref_prog_spec_alt with "[//]") as "_".
    wp_pures.
    (** We can prefix a tactic with [by] to resolve remaining goals using [done],
    i.e., [by tac] is syntactic sugar for [tac; done]. *)
    by iApply "HΦ".
  Qed.

  Lemma fork_prog_spec_alt :
    {{{ True }}} fork_prog #() {{{ RET SOMEV #42; True }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_lam. wp_alloc l as "Hl". wp_store. wp_load.
    (** In addition to [wp_apply], there is also [wp_smart_apply], which
    performs pure reductions until the lemma is applicable. Here we need that to
    reduce the [let]. *)
    wp_smart_apply (wp_fork with "[Hl]").
    { iModIntro. by wp_free. }
    wp_pures. by iApply "HΦ".
  Qed.
End proof.
