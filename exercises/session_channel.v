From iris_tutorial Require Export one_shot.

(**                             *)
(** Functional Session Channels *)
(**                             *)

Definition new : val := new1.
Definition send : val := λ: "c1" "v",
  let: "c2" := new1 #() in
  send1 "c1" ("v", "c2");; "c2".
Definition recv : val := recv1.
Definition close : val := λ: "c",
  send1 "c" #().
Definition wait : val := recv1.

Section session_proofs.
  Context `{!heapGS Σ, !chanG Σ}.

  (** !x <v> {P}. p *)
  Definition send_prot {A} (v : A → val) (Φ : A -d> iProp Σ) (p : A → prot Σ) : prot Σ :=
    (true, λ r, ∃ x ch', ⌜r = (v x, ch')%V⌝ ∗ Φ x ∗ is_chan ch' (dual (p x)))%I.

  (** ?x <v> {P}. p *)
  Definition recv_prot {A} (v : A → val) (Φ : A → iProp Σ) (p : A → prot Σ) : prot Σ :=
    dual (send_prot v Φ (dual ∘ p)).

  (** End? *)
  Definition wait_prot (P : iProp Σ) : prot Σ := (false, (λ r, ⌜r = #()⌝ ∗ P)%I).
  (** End! *)
  Definition close_prot (P : iProp Σ) : prot Σ := dual (wait_prot P).

  Lemma new_spec p :
    {{{ True }}} new #() {{{ ch, RET ch; is_chan ch p ∗ is_chan ch (dual p) }}}.
  Proof. apply new1_spec. Qed.

  Lemma recv_spec {A} ch (v : A → val) Φ p :
    {{{ is_chan ch (recv_prot v Φ p) }}}
      recv ch
    {{{ x ch', RET (v x, ch'); is_chan ch' (p x) ∗ Φ x }}}.
  Proof.
    iIntros (Ψ) "Hr HΨ". wp_apply (recv1_spec with "Hr").
    iIntros (ch') "(%x&%w&->&?&?)". iApply "HΨ". rewrite dual_dual. iFrame.
  Qed.

  Lemma send_spec {A} x ch (v : A → val) Φ p :
    {{{ is_chan ch (send_prot v Φ p) ∗ ▷ Φ x }}}
      send ch (v x)
    {{{ ch', RET ch'; is_chan ch' (p x) }}}.
  Proof.
    (* exercise *)
  Admitted.

  Lemma wait_spec ch P :
    {{{ is_chan ch (wait_prot P) }}} wait ch {{{ RET #(); P }}}.
  Proof.
    (* exercise *)
  Admitted.

  Lemma close_spec ch P :
    {{{ is_chan ch (close_prot P) ∗ P }}} close ch {{{ RET #(); emp }}}.
  Proof.
    (* exercise *)
  Admitted.

  (** Typeclasses for guarded recursion, mostly a technicality. *)
  Global Instance recv_prot_contractive A n :
    Proper (pointwise_relation A (dist n) ==>
            pointwise_relation A (dist n) ==>
            pointwise_relation A (dist_later n) ==> dist n) recv_prot.
  Proof.
    intros v1 v2 Hveq Φ1 Φ2 HΦeq p1 p2 Hpeq. rewrite /recv_prot.
    split; [done|]=> /= v. do 6 f_equiv; [by rewrite Hveq|done|].
    apply is_chan_contractive. rewrite !dual_dual. apply Hpeq.
  Qed.
  Global Instance recv_prot_ne A n :
    Proper (pointwise_relation A (dist n) ==>
            pointwise_relation A (dist n) ==>
            pointwise_relation A (dist n) ==> dist n) recv_prot.
  Proof.
    intros v1 v2 Hveq Φ1 Φ2 HΦeq p1 p2 Hpeq. rewrite /recv_prot.
    split; [done|]=> /= v. do 6 f_equiv; [by rewrite Hveq|done|].
    apply is_chan_ne. rewrite !dual_dual. apply Hpeq.
  Qed.
  Global Instance recv_prot_proper A :
    Proper (pointwise_relation A (≡) ==>
            pointwise_relation A (≡) ==>
            pointwise_relation A (≡) ==> (≡)) recv_prot.
  Proof.
    intros v1 v2 Hveq Φ1 Φ2 HΦeq p1 p2 Hpeq. rewrite /recv_prot.
    split; [done|]=> /= v. do 6 f_equiv; [by rewrite Hveq|done|].
    apply is_chan_proper. rewrite !dual_dual. apply Hpeq.
  Qed.

  Global Instance send_prot_contractive A n :
    Proper (pointwise_relation A (dist n) ==>
            pointwise_relation A (dist n) ==>
            pointwise_relation A (dist_later n) ==> dist n) send_prot.
  Proof.
    intros v1 v2 Hveq Φ1 Φ2 HΦeq p1 p2 Hpeq. rewrite /send_prot.
    split; [done|]=> /= v. do 6 f_equiv; [by rewrite Hveq|done|].
    f_contractive. simpl. f_equiv. by apply Hpeq.
  Qed.
  Global Instance send_prot_ne A n :
    Proper (pointwise_relation A (dist n) ==>
            pointwise_relation A (dist n) ==>
            pointwise_relation A (dist n) ==> dist n) send_prot.
  Proof.
    intros v1 v2 Hveq Φ1 Φ2 HΦeq p1 p2 Hpeq. rewrite /send_prot.
    split; [done|]=> /= v. do 6 f_equiv; [by rewrite Hveq|done|].
    apply is_chan_ne. simpl. by f_equiv.
  Qed.
  Global Instance send_prot_proper A :
    Proper (pointwise_relation A (≡) ==>
            pointwise_relation A (≡) ==>
            pointwise_relation A (≡) ==> (≡)) send_prot.
  Proof.
    intros v1 v2 Hveq Φ1 Φ2 HΦeq p1 p2 Hpeq. rewrite /send_prot.
    split; [done|]=> /= v. do 6 f_equiv; [by rewrite Hveq|done|].
    apply is_chan_proper. simpl. by f_equiv.
  Qed.

  Lemma recv_prot_dual {A} (v : A → val) Φ p :
    dual (recv_prot v Φ p) ≡ send_prot v Φ (dual∘p).
  Proof. done. Qed.
  Lemma send_prot_dual {A} (v : A → val) Φ p :
    dual (send_prot v Φ p) ≡ recv_prot v Φ (dual∘p).
  Proof. split; [done|]=> x /=. do 7 f_equiv. by rewrite dual_dual. Qed.
  Lemma wait_prot_dual P :
    dual (wait_prot P) ≡ close_prot P.
  Proof. done. Qed.
  Lemma close_prot_dual P :
    dual (close_prot P) ≡ wait_prot P.
  Proof. done. Qed.
End session_proofs.

Notation "<? x > 'MSG' v {{ P } } ; p" :=
  (recv_prot (λ x, v) (λ x, P)%I (λ x, p)) (x binder, at level 200).
Notation "<! x > 'MSG' v {{ P } } ; p" :=
  (send_prot (λ x, v) (λ x, P)%I (λ x, p)) (x binder, at level 200).

Notation "<? x > 'MSG' v ; p" :=
  (<? x> MSG v {{ True }} ; p) (x binder, at level 200).
Notation "<! x > 'MSG' v ; p" :=
  (<! x> MSG v {{ True }} ; p) (x binder, at level 200).

Notation "<?> 'MSG' v {{ P } } ; p" :=
  (<? (_:unit)> MSG v {{ P }} ; p) (at level 200).
Notation "<!> 'MSG' v {{ P } } ; p" :=
  (<! (_:unit)> MSG v {{ P }} ; p) (at level 200).

Notation "<?> 'MSG' v ; p" :=
  (<?> MSG v {{ True }} ; p) (at level 200).
Notation "<!> 'MSG' v ; p" :=
  (<!> MSG v {{ True }} ; p) (at level 200).

Notation "<!> 'END' {{ P } }" := (close_prot P).
Notation "<?> 'END' {{ P } }" := (wait_prot P).
Notation "<!> 'END'" := (close_prot True).
Notation "<?> 'END'" := (wait_prot True).

(**                  *)
(** Session Channels *)
(**                  *)

Definition new_imp : val := λ: <>,
  let: "c" := new #() in
  (ref "c", ref "c" ).
Definition recv_imp : val := λ: "c",
  let: "r" := recv (!"c") in
  "c" <- (Snd "r");; Fst "r".
Definition send_imp : val := λ: "c" "v",
  "c" <- send (!"c") "v".
Definition close_imp : val := λ: "c",
  close (!"c");; Free "c".
Definition wait_imp : val := λ: "c",
  wait (!"c");; Free "c".

Section imp_proofs.
  Context `{!heapGS Σ, !chanG Σ}.

  Definition is_chan_imp (ch : val) (p : prot Σ) : iProp Σ :=
    ∃ (l : loc) ch', ⌜ch = #l⌝ ∗ l ↦ ch' ∗ is_chan ch' p.

  Global Instance is_chan_imp_contractive ch : Contractive (is_chan_imp ch).
  Proof.
    intros n [??] [??] Hpair; solve_proper_prepare.
    by repeat first [f_contractive; destruct Hpair; simplify_eq/=| f_equiv].
  Qed.
  Global Instance is_chan_imp_ne ch : NonExpansive (is_chan_imp ch).
  Proof. solve_proper. Qed.
  Global Instance is_chan_imp_proper ch : Proper ((≡) ==> (≡)) (is_chan_imp ch).
  Proof. solve_proper. Qed.

  Lemma new_imp_spec p :
    {{{ True }}}
      new_imp #()
    {{{ ch1 ch2, RET (ch1,ch2); is_chan_imp ch1 p ∗ is_chan_imp ch2 (dual p) }}}.
  Proof.
    (* exercise *)
  Admitted.

  Lemma recv_imp_spec {A} ch (v : A → val) Φ p :
    {{{ is_chan_imp ch (recv_prot v Φ p) }}}
      recv_imp ch
    {{{ x, RET v x; is_chan_imp ch (p x) ∗ Φ x }}}.
  Proof.
    (* exercise *)
  Admitted.

  Lemma send_imp_spec {A} x ch (v : A → val) Φ p :
    {{{ is_chan_imp ch (send_prot v Φ p) ∗ ▷ Φ x }}}
      send_imp ch (v x)
    {{{ RET #(); is_chan_imp ch (p x) }}}.
  Proof.
    (* exercise *)
  Admitted.

  Lemma wait_imp_spec ch P :
    {{{ is_chan_imp ch (wait_prot P) }}}
      wait_imp ch
    {{{ RET #(); P }}}.
  Proof.
    (* exercise *)
  Admitted.

  Lemma close_imp_spec ch P :
    {{{ is_chan_imp ch (close_prot P) ∗ P }}}
      close_imp ch
    {{{ RET #(); emp }}}.
  Proof.
    (* exercise *)
  Admitted.
End imp_proofs.

Notation "c ↣ p" :=
  (is_chan_imp c p) (at level 20, format "c  ↣  p") : bi_scope.
